<!-- The Modal -->
<div class="modal" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Login</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body align-items-center">

                <div class="col-md-7 row-block">
                    <a class="btn btn-lg btn-primary btn-block" href="{{ url('auth/google') }}">
                        <strong>Login With Google</strong>
                    </a>
                </div>
                <div class="col-md-7 row-block">
                    <a class="btn btn-lg btn-primary btn-block" href="{{ url('auth/redirect/facebook') }}">
                        <strong>Login With Facebook</strong>
                    </a>
                </div>


                <div class="col-md-7 row-block">
                    <a class="btn btn-lg btn-primary btn-block" href="{{ url('auth/redirect/twitter') }}">
                        <strong>Login With Twitter</strong>
                    </a>
                </div>

                <div class="col-md-7 row-block">
                    <a class="btn btn-lg btn-primary btn-block" href="{{ url('line') }}">
                        <strong>Login With Line</strong>
                    </a>
                </div>


            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>


<style>
    .align-items-center {}

    .row-block {
        padding: 8px 10px;
        margin-left: 100px;
    }

    .modal-body {
        height: 275px;

    }
</style>