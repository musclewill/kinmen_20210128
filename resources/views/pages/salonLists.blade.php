@extends('layouts.default')
@section('content')

<div class="row album py-5 bg-light">
  @foreach($salonLists as $salonList)
  <div class="col-md-4 box-margin">
    <div class="card mb-4 box-shadow">
      <img class="card-img-top" src="{{ Storage::url( $salonList->img_url_1 ) }}" alt="Card image cap">
      <div class="card-body">
        <p class="card-text">{{ $salonList->title }}</p>
        <p class="card-text">{{ $salonList->content }}</p>
        <p class="card-text">{{ $salonList->address }}</p>

        <div class="d-flex justify-content-between align-items-center">
          <div class="btn-group">
            <button type="button" class="btn btn-sm btn-outline-secondary">View</button>
            <button type="button" class="btn btn-sm btn-outline-secondary">Edit</button>
          </div>
          <small class="text-muted">9 mins</small>
        </div>
      </div>
    </div>
  </div>
  @endforeach
</div>


          <div class="row album py-5 bg-light">
            <div class="col-md-4 box-margin">
              <div class="card mb-4 box-shadow">
                <img class="card-img-top" src="{{ Storage::url( $salonLists[0]->img_url_1 ) }}" alt="Card image cap">
                <div class="card-body">
                  <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                  <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group">
                      <button type="button" class="btn btn-sm btn-outline-secondary">View</button>
                      <button type="button" class="btn btn-sm btn-outline-secondary">Edit</button>
                    </div>
                    <small class="text-muted">9 mins</small>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4 box-margin">
              <div class="card mb-4 box-shadow">
                <img class="card-img-top" src="{{ Storage::url( $salonLists[0]->img_url_1 ) }}" alt="Card image cap">
                <div class="card-body">
                  <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                  <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group">
                      <button type="button" class="btn btn-sm btn-outline-secondary">View</button>
                      <button type="button" class="btn btn-sm btn-outline-secondary">Edit</button>
                    </div>
                    <small class="text-muted">9 mins</small>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4 box-margin">
              <div class="card mb-4 box-shadow">
                <img class="card-img-top" src="{{ Storage::url( $salonLists[0]->img_url_1 ) }}" alt="Card image cap">
                <div class="card-body">
                  <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                  <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group">
                      <button type="button" class="btn btn-sm btn-outline-secondary">View</button>
                      <button type="button" class="btn btn-sm btn-outline-secondary">Edit</button>
                    </div>
                    <small class="text-muted">9 mins</small>
                  </div>
                </div>
              </div>
            </div>
          </div>



<!-- The Modal -->
<div class="modal" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">
        
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Login</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            
            <!-- Modal body -->
            <div class="modal-body align-items-center">

                    <div class="col-md-7 row-block">
                        <a class="btn btn-lg btn-primary btn-block" href="{{ url('auth/google') }}">
                            <strong>Login With Google</strong>
                        </a>
		    </div>

                    <div class="col-md-7 row-block">
                        <a class="btn btn-lg btn-primary btn-block" href="{{ url('auth/redirect/facebook') }}">
                            <strong>Login With Facebook</strong>
                        </a>
                    </div>

                    <div class="col-md-7 row-block">
                        <a class="btn btn-lg btn-primary btn-block" href="{{ url('auth/redirect/twitter') }}">
                            <strong>Login With Twitter</strong>
                        </a>
                    </div>

            </div>
            
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        
        </div>
    </div>
</div>
@endsection
<style>
.align-items-center {

}
.row-block {
	padding: 8px 10px;
	margin-left: 100px;
}

.modal-body {
	height: 230px;
}


.card-img-top {
  width: 100%;
  min-height: 210px;
  max-height: 210px;
}

.card-body {
  min-height: 170px;
}

.box-margin {
  max-height: 340px;
}

@media (min-width: 768px) {
  .box-margin {
    margin-bottom: 65px;
  }
}

@media (min-width: 500px) {
  .box-margin {
    margin-bottom: 75px;
  }
}

@media (min-width: 300px) {
  .box-margin {
    margin-bottom: 75px;
    padding-left: 30px!important;
  }
}


.box-shadow { box-shadow: 0 .25rem .75rem #333; }


</style>

