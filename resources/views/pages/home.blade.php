@extends('layouts.default')
@include('auth.social_login')
@section('content')
<div class="gallery-container section-container">
	<div class="container">
		<div class="row">
			 <div class="col gallery section-description wow fadeIn">
				<h2>Gallery</h2>
				<div class="divider-1 wow fadeInUp"><span></span></div>
			</div>

			@foreach ($users_rooms as $key => $users_room)
			<div class="col-md-4 gallery-box wow fadeInDown">
				<div data-toggle="modal">
					<a href="{{route('room.show', $users_room['id'])}}">
						@isset($users_room->pic_1)
						<img class="image1" src="{{ asset('storage/images/'.$users_room->pic_1) }}" alt="Image">
						@else
						<img class="image1" src="https://www.kinmen-island.xyz/storage/specific/no_pic.jpg" alt="Image">
						@endisset
					</a>
				</div>
				<div>{{ $users_room['address'] }}</div>
				<div>{{ $users_room['related_address'] }}</div>
			</div>
			@endforeach
		</div>
		{!! $users_rooms->links() !!}
	</div>
</div>

@endsection
<style>
.align-items-center {

}
.row-block {
	padding: 8px 10px;
	margin-left: 100px;
}

.modal-body {
	height: 230px;

}

.image1 {
	width: 100%;
	height: 30%;
}

</style>

