@extends('layouts.default')
@include('auth.social_login')
@section('content')
<div>
        <form method="post" action="{{ url('storeUserRoom') }}" enctype="multipart/form-data">
                <!-- CROSS Site Request Forgery Protection -->
                @csrf

                <div class="form-group">
                        <label>地址</label>
                        <!-- <label>Address</label> -->
                        <input type="text" class="form-control {{ $errors->has('address') ? 'error' : '' }}" name="address" id="address">

                        <!-- Error -->
                        @if ($errors->has('address'))
                        <div class="error">
                                {{ $errors->first('address') }}
                        </div>
                        @endif
                </div>

                <div class="form-group">
                        <label>相對位址描述</label>
                        <!-- <label>Related Address</label> -->
                        <input type="text" class="form-control {{ $errors->has('related_address') ? 'error' : '' }}" name="related_address" id="related_address">

                        <!-- Error -->
                        @if ($errors->has('related_address'))
                        <div class="error">
                                {{ $errors->first('related_address') }}
                        </div>
                        @endif
                </div>

                <div class="form-group">
                        <label>Google 地圖 連結 (請勿縮短網址)</label>
                        <!-- <label>Google map url</label> -->
                        <input type="text" class="form-control {{ $errors->has('google_map_url') ? 'error' : '' }}" name="google_map_url" id="google_map_url">

                        <!-- Error -->
                        @if ($errors->has('google_map_url'))
                        <div class="error">
                                {{ $errors->first('google_map_url') }}
                        </div>
                        @endif
                </div>

                <div class="form-group">
                        <label>設備</label>
                        <!-- <label>Equipment</label> -->
                        <input type="text" class="form-control {{ $errors->has('equipments') ? 'error' : '' }}" name="equipments" id="equipments">

                        <!-- Error -->
                        @if ($errors->has('equipments'))
                        <div class="error">
                                {{ $errors->first('equipments') }}
                        </div>
                        @endif
                </div>

                <div class="form-group">
                        <label>規則</label>
                        <!-- <label>Rules</label> -->
                        <textarea class="form-control {{ $errors->has('rules') ? 'error' : '' }}" name="rules" id="rules" rows="4"></textarea>

                        <!-- Error -->
                        @if ($errors->has('rules'))
                        <div class="error">
                                {{ $errors->first('rules') }}
                        </div>
                        @endif
                </div>

                <h3 class="text-left mb-5">上傳資料</h3>
                <!-- <h3 class="text-center mb-5">Upload File in Laravel</h3> -->
                @if($message = Session::get('success'))
                <div class="alert alert-success">
                        <strong>{{ $message }}</strong>
                </div>
                @endif

                @if (count($errors) > 0)
                <div class="alert alert-danger">
                        <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                        </ul>
                </div>
                @endif

                <div class="custom-file">
                        <input type="file" name="pic_1" class="custom-file-input" id="choosePic1">
                        <label class="custom-file-label" for="chooseFile">圖片1</label>
                        <!-- <label class="custom-file-label" for="chooseFile">Select Picture1</label> -->
                </div>

                <div class="custom-file">
                        <input type="file" name="pic_2" class="custom-file-input" id="choosePic2">
                        <label class="custom-file-label" for="chooseFile">圖片2</label>
                        <!-- <label class="custom-file-label" for="chooseFile">Select Picture2</label> -->
                </div>

                <input type="submit" name="send" value="送出" class="btn btn-dark btn-block">
                <!-- <input type="submit" name="send" value="Submit" class="btn btn-dark btn-block"> -->
        </form>
</div>

@endsection