@extends('layouts.default')
@include('auth.social_login')
@section('content')
<div>
    <button class="button login-button"><a href="{{ url('/formUserRoom') }}">上傳</a></button>
</div>

@if( count($users_rooms) > 0 )
<table class="table table-hover">
    <thead>
        <tr>
            <!-- <th>Room number</th> -->
            <!-- <th>Addres</th> -->
            <!-- <th>ID</th> -->
            <!-- <th>Related Address</th> -->
            <!-- <th>Update</th> -->
            <th>房號</th>
            <th>地址</th>
            <th>ID</th>
            <th>相對位址描述</th>
            <th>詳細資料</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($users_rooms as $key => $users_room)
        <tr>
            <td> {{ $key+1 }} </td>
            <td> {{ $users_room['address'] }} </td>
            <td> {{ $users_room['id'] }} </td>
            <td> {{ $users_room['related_address'] }} </td>

            <td>
                <div class="d-flex">
                    <!-- <a href="{{route('usersroom.edit', $users_room['id'])}}" class="btn btn-sm btn-info">Edit</a> -->
                    <!-- <a href="{{ route('usersroom.delete',  $users_room['id']) }}" class="btn btn-sm btn-danger">Delete</a> -->
                    <a href="{{route('usersroom.show', $users_room['id'])}}" class="btn btn-sm btn-success">詳細</a>
                </div>
            </td>

        </tr>
        @endforeach
    </tbody>
</table>

{!! $users_rooms->links() !!}
@else

目前沒有資料 請點選上傳 新增資料

@endif


@endsection