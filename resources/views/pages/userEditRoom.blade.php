@extends('layouts.default')
@include('auth.social_login')
@section('content')
<div>
	<form  method="post" action="{{ route('usersroom.update', $user_room->id) }}" enctype="multipart/form-data">
            <!-- CROSS Site Request Forgery Protection -->
	    @csrf

            <div class="form-group">
                <label>Address</label>
                <input type="text" class="form-control {{ $errors->has('address') ? 'error' : '' }}" name="address" id="address" value="{{$user_room->address}}">

                <!-- Error -->
                @if ($errors->has('address'))
                <div class="error">
                        {{ $errors->first('address') }}
                </div>
                @endif
	    </div>



            <div class="form-group">
                <label>Related Address</label>
                <input type="text" class="form-control {{ $errors->has('related_address') ? 'error' : '' }}" name="related_address" id="related_address" value="{{$user_room->related_address}}">

                <!-- Error -->
                @if ($errors->has('related_address'))
                <div class="error">
                        {{ $errors->first('related_address') }}
                </div>
                @endif
	    </div>


            <div class="form-group">
                <label>Google map url</label>
                <input type="text" class="form-control {{ $errors->has('google_map_url') ? 'error' : '' }}" name="google_map_url" id="google_map_url" value="{{$user_room->google_map_url}}">

                <!-- Error -->
                @if ($errors->has('google_map_url'))
                <div class="error">
                        {{ $errors->first('google_map_url') }}
                </div>
                @endif
	    </div>

            <div class="form-group">
                <label>Equipment</label>
                <input type="text" class="form-control {{ $errors->has('equipments') ? 'error' : '' }}" name="equipments" id="equipments" value="{{$user_room->equipments}}">

                <!-- Error -->
                @if ($errors->has('equipments'))
                <div class="error">
                        {{ $errors->first('equipments') }}
                </div>
                @endif
	    </div>

            <div class="form-group">
                <label>Rules</label>
                <textarea class="form-control {{ $errors->has('rules') ? 'error' : '' }}" name="rules" id="rules" rows="4"> {{$user_room->rules}} </textarea>

                <!-- Error -->
                @if ($errors->has('rules'))
                <div class="error">
                        {{ $errors->first('rules') }}
                </div>
                @endif
	    </div>

            <div class="form-group">
                <label>Picture 1</label>
                <img src="{{ asset('storage/images/'.$user_room->pic_1) }}" alt="pic_1" title="pic_1" class="form-img {{ $errors->has('pic_1') ? 'error' : '' }}" id="pic_1" />

                <!-- Error -->
                @if ($errors->has('pic_1'))
                <div class="error">
                        {{ $errors->first('pic_1') }}
                </div>
                @endif

                <input type="file" name="pic_1" class="custom-file-input" id="choosePic1">
		<label class="custom-file-label" for="chooseFile">Select Picture1</label>

            </div>

            <div class="form-group">
                <label>Picture 2</label>
                <img src="{{ asset('storage/images/'.$user_room->pic_2) }}" alt="pic_2" title="pic_2" class="form-img {{ $errors->has('pic_1') ? 'error' : '' }}" id="pic_2" />

                <!-- Error -->
                @if ($errors->has('pic_2'))
                <div class="error">
                        {{ $errors->first('pic_2') }}
                </div>
                @endif

                <input type="file" name="pic_2" class="custom-file-input" id="choosePic2">
		<label class="custom-file-label" for="chooseFile">Select Picture2</label>


	    </div>

	    <!-- input type="submit" name="send" value="Submit" class="btn btn-dark btn-block" -->

	    <div class="edit-submit"><a href="{{$previous_url}}" class="btn btn-block btn-sm btn-success">Back</a></div>
	    <div class="edit-back"><input type="submit" name="send" value="Submit" class="btn btn-dark btn-block"></div>


        </form>
</div>

@endsection

<style>
.row {
	margin-top: 5%;
}

.form-img {
        width:100%;
        height:100px;
}

.edit-submit , .edit-back {
        margin:1% 25%;
}


</style>
