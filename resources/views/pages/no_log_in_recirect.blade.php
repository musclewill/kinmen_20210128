@extends('layouts.default')
@include('auth.social_login')
@section('content')


<div class="no_log_in modal-body align-items-center">
	<div class="col-md-7 row-block">
		<a class="btn btn-lg btn-primary btn-block" href="{{ url('auth/google') }}">
			<strong>Login With Google</strong>
		</a>
	</div>
	<div class="col-md-7 row-block">
		<a class="btn btn-lg btn-primary btn-block" href="{{ url('auth/redirect/facebook') }}">
			<strong>Login With Facebook</strong>
		</a>
	</div>
	<div class="col-md-7 row-block">
		<a class="btn btn-lg btn-primary btn-block" href="{{ url('auth/redirect/twitter') }}">
			<strong>Login With Twitter</strong>
		</a>
	</div>
</div>




@endsection

<style>

.no_log_in {
	margin-top: 10%;
}

</style>

