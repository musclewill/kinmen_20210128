@extends('layouts.default')
@include('auth.social_login')
@section('content')
<div>
	<form  method="post" action="{{ route('usersroom.update', $user_room->id) }}">
            <!-- CROSS Site Request Forgery Protection -->
	    @csrf

            <div class="form-group">
                <label>Address</label>
                <input type="text" class="form-control {{ $errors->has('address') ? 'error' : '' }}" name="address" id="address" value="{{$user_room->address}}">

                <!-- Error -->
                @if ($errors->has('address'))
                <div class="error">
                        {{ $errors->first('address') }}
                </div>
                @endif
	    </div>



            <div class="form-group">
                <label>Related Address</label>
                <input type="text" class="form-control {{ $errors->has('related_address') ? 'error' : '' }}" name="related_address" id="related_address" value="{{$user_room->related_address}}">

                <!-- Error -->
                @if ($errors->has('related_address'))
                <div class="error">
                        {{ $errors->first('related_address') }}
                </div>
                @endif
	    </div>


            <div class="form-group">
                <label>Google map url</label>
                <input type="text" class="form-control {{ $errors->has('google_map_url') ? 'error' : '' }}" name="google_map_url" id="google_map_url" value="{{$user_room->google_map_url}}">

                <!-- Error -->
                @if ($errors->has('google_map_url'))
                <div class="error">
                        {{ $errors->first('google_map_url') }}
                </div>
                @endif
	    </div>

            <div class="form-group">
                <label>Equipment</label>
                <input type="text" class="form-control {{ $errors->has('equipments') ? 'error' : '' }}" name="equipments" id="equipments" value="{{$user_room->equipments}}">

                <!-- Error -->
                @if ($errors->has('equipments'))
                <div class="error">
                        {{ $errors->first('equipments') }}
                </div>
                @endif
	    </div>

            <div class="form-group">
                <label>Rules</label>
                <textarea class="form-control {{ $errors->has('rules') ? 'error' : '' }}" name="rules" id="rules" rows="4"> {{$user_room->rules}} </textarea>

                <!-- Error -->
                @if ($errors->has('rules'))
                <div class="error">
                        {{ $errors->first('rules') }}
                </div>
                @endif
	    </div>

            <div class="form-group">
                <label>Picture 1</label>
		<img src="{{ asset('storage/images/'.$user_room->pic_1) }}" alt="pic_1" title="pic_1" class="form-img {{ $errors->has('pic_1') ? 'error' : '' }}" name="pic_1" id="pic_1" />

                <!-- Error -->
                @if ($errors->has('pic_1'))
                <div class="error">
                        {{ $errors->first('pic_1') }}
                </div>
                @endif
            </div>

            <div class="form-group">
                <label>Picture 2</label>
		<img src="{{ asset('storage/images/'.$user_room->pic_2) }}" alt="pic_2" title="pic_2" class="form-img {{ $errors->has('pic_2') ? 'error' : '' }}" name="pic_2" id="pic_2" />

                <!-- Error -->
                @if ($errors->has('pic_2'))
                <div class="error">
                        {{ $errors->first('pic_2') }}
                </div>
                @endif
            </div>


	    <div id="map-canvas"></div>


<!-- iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d14460.510025234189!2d121.5679626!3d25.0297466!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3442abb6da80a7ad%3A0xacc4d11dc963103c!2sTAIPEI%20101!5e0!3m2!1sen!2stw!4v1610942200785!5m2!1sen!2stw" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe -->



<!-- div class="map-container">
	<iframe width="100%" height="450" frameborder="0" scrolling="no" marginheight="0"
          marginwidth="0" src="https://maps.google.com/maps?q=Rashmoni Ave, Maidan, Kolkata, West Bengal 700069, 
         India, &t=&z=14&ie=UTF8&iwloc=&output=embed"></iframe>
</div -->



	    <div class="detail-edit"><a href="{{route('usersroom.edit', $user_room->id)}}" class="btn btn-block btn-sm btn-info">Edit</a></div>
	    <!-- div><a href="{{route('usersroom.edit', $user_room->id)}}" class="btn btn-sm btn-info">Edit</a></div -->
	    <div class="detail-back"><a href="{{$previous_url}}" class="btn btn-block btn-sm btn-success">Back</a></div>
        </form>
</div>

@endsection

<script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
<script>

var str_cor = '{{$user_room->coordinates}}';
var res = str_cor.split(",");
//console.log(typeof(jobs));


var map;
var myLatLng = {lat: res[0], lng: res[1]};
//var myLatLng = {lat: 25.1350625, lng: 121.6516873};

var lat = '{{$lat}}';
var lon = '{{$lon}}';

function initialize() {
        var mapOptions = {
                zoom: 13,
		// center: new google.maps.LatLng(res[0],res[1])
		center: new google.maps.LatLng(lat, lon)

		//center: new google.maps.LatLng(25.1350625,121.6516873)
                //center: myLatLng

        };
        map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
}

google.maps.event.addDomListener(window, 'load', initialize);

</script>


<style>
.row {
	margin-top: 5%;
}
.form-img {
	width:100%;
	height:100px;
}

.detail-edit , .detail-back {
	margin:1% 25%;
}

#map-canvas {
	height: 50%;
	margin: 0px;
        padding: 0px;
        border: solid;
}

</style>
