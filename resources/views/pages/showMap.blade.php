@extends('layouts.default')
@include('auth.social_login')
@section('content')
  <table class="table table-hover">
    <thead>
      <tr>
        <th>Room 23 number</th>
        <th>Addres</th>
	<th>ID</th>
	<th>Related Address</th>
	<th>Update</th>
      </tr>
    </thead>
  </table>


<div id="map"></div>

<ul id="geoData">
    <li>Latitude: <span id="lat-span"></span></li>
    <li>Longitude: <span id="lon-span"></span></li>
</ul>

@endsection
<script>
function initMap() {
	    var myLatLng = {lat: 22.3038945, lng: 70.80215989999999};
	      
	        var map = new google.maps.Map(document.getElementById('map'), {
		      center: myLatLng,
	            zoom: 13
	        });
	      
    var marker = new google.maps.Marker({
          position: myLatLng,
          map: map,
          title: 'Hello World!',
          draggable: true
        });
  
     google.maps.event.addListener(marker, 'dragend', function(marker) {
        var latLng = marker.latLng;
        document.getElementById('lat-span').innerHTML = latLng.lat();
        document.getElementById('lon-span').innerHTML = latLng.lng();
     });
}
</script>

<script src="https://maps.googleapis.com/maps/api/js?libraries=places&callback=initMap" async defer></script>

<style>
      #map-canvas, #map {
        height: 100%;
        margin: 0px;
	padding: 0px;
	border: solid;
      }
</style>


