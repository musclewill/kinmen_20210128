@extends('layouts.default')
@include('auth.social_login')
@section('content')

  <table class="table table-hover">
    <thead>
      <tr>
        <th>Firstname</th>
        <th>Lastname</th>
        <th>Email</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($users as $user)
      <tr>
        <td> {{ $user['name'] }} </td>
        <td> {{ $user['id'] }}  </td>
        <td> {{ $user['email'] }}  </td>
      </tr>
      @endforeach

    </tbody>
  </table>

@endsection
