<?php

namespace App\Admin\Controllers;

use App\SalonLists;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class SalonListsController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'SalonLists';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new SalonLists());

        $grid->column('id', __('Id'));
        $grid->column('title', __('Title'));
        $grid->column('content', __('Content'));
        $grid->column('owner_id', __('Owner id'));
        $grid->column('address', __('Address'));
        $grid->column('img_url_1', __('Img url 1'));
        $grid->column('img_url_2', __('Img url 2'));
        $grid->column('img_url_3', __('Img url 3'));
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(SalonLists::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('title', __('Title'));
        $show->field('content', __('Content'));
        $show->field('owner_id', __('Owner id'));
        $show->field('address', __('Address'));
        $show->field('img_url_1', __('Img url 1'));
        $show->field('img_url_2', __('Img url 2'));
        $show->field('img_url_3', __('Img url 3'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new SalonLists());

        $form->text('title', __('Title'));
        $form->text('content', __('Content'));
        $form->number('owner_id', __('Owner id'));
        $form->text('address', __('Address'));
        $form->text('img_url_1', __('Img url 1'));
        $form->text('img_url_2', __('Img url 2'));
        $form->text('img_url_3', __('Img url 3'));

        return $form;
    }
}
