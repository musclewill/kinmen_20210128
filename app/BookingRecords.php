<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookingRecords extends Model
{
    //
    protected $fillable = [
        'owner_id', 'member_id', 'date', 'hour', 'minute',
    ];


}
