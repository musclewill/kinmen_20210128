<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersRooms extends Model
{
    protected $fillable = [
        'user_id', 'address', 'related_address', 'google_map_url', 'coordinates',  'equipments', 'rules', 'sun_s',  'sun_e', 'mon_s', 'mon_e', 'tue_s', 'tue_e', 'wed_s', 'wed_e', 'thu_s', 'thu_e', 'fri_s', 'fri_e', 'sat_s', 'sat_e', 'pic_1', 'pic_2', 'pic_3', 'pic_4', 'pic_5'
    ];


}
