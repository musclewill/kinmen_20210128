<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\User as Users;
use App\UsersRooms as UsersRooms;
use Auth;
use Storage;


class UserRoomsController extends Controller
{
	private $pic_count = 5;

	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */

	public function index()
	{
		if (auth()->check()) {
			//$users_rooms = UsersRooms::paginate(4);
			//$users_rooms = UsersRooms::orderBy('created_at', 'desc')->paginate(5);
			$users_rooms = UsersRooms::orderBy('created_at', 'desc')
				->where('user_id', '=', Auth::id())->paginate(4);
			
			return view('pages.userRoomsList', compact('users_rooms'));
		} else {
			return response()
				->view('pages.no_log_in_recirect');
			//return redirect('/');
		}
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		if (Auth::check()) {
			return response()
				->view('pages.userFormRoom');
		} else {
			return response()
				->view('pages.no_log_in_recirect');
		}
	}

	private function googleMapUrlVal($url)
	{
		$tmp1 = strrchr($url, "@");
		$tmp2 = explode(",", $tmp1);

		if ($tmp1) {
			if (is_numeric(substr($tmp2[0], 1)) && is_numeric($tmp2[1])) {
				//return TRUE;
				//return array( substr($tmp2[0],1), $tmp2[1] );
				return substr($tmp2[0], 1) . ',' . $tmp2[1];
			} else {
				return FALSE;
			}
		} else {
			return FALSE;
		}
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		// $validator = Validator::make($request->all(), [
		// 	'address' => 'required|string|min:3',
		// 	'related_address' => 'required|string|min:3',
		// 	'pic_1' => 'mimes:jpeg,png,jpg,gif,pdf|max:2048',
		// 	'pic_2' => 'mimes:jpeg,png,jpg,gif,pdf|max:2048',
		// ]);

		if (!Auth::check()) {
			return response()
				->view('pages.no_log_in_recirect');
		}

		$validator = Validator::make(
			$request->all(),
			[
				'address' => 'required|string|min:3',
				'related_address' => 'required|string|min:3',
				//'pic_1' => 'required|mimes:jpeg,png,jpg,gif,pdf|max:2048',
				'pic_1' => 'mimes:jpeg,png,jpg,gif,pdf|max:2048',
				'pic_2' => 'mimes:jpeg,png,jpg,gif,pdf|max:2048',
				// 'google_map_url' => [
					//'required',
					// 'string',
					// function ($attribute, $value, $fail) {
					// 	$res = $this->googleMapUrlVal($value);

					// 	if (!$res) {
					// 		$fail('Please enter the whole google map url with latitude&longitude on field ' . $attribute);
					// 	} //else { }
					// },
				// ],
			],
			[
				'address.required' => 'Address is required............',
				'pic_1.required' => 'pic_1 is required............',
				//'pic_2.required' => 'pic_2 is required............',
			]
		);



		if ($validator->fails()) {
			return redirect()->Back()->withInput()->withErrors($validator);
		}

		$user_id = Auth::id();
		$req =  $request->all();

		$usersRooms = new UsersRooms([
			'user_id' => $user_id,
			'address' => $req['address'],
			'related_address' => $req['related_address'],
			'google_map_url' => $req['google_map_url'],
			'coordinates' => $this->googleMapUrlVal($req['google_map_url']),
			'equipments' => $req['equipments'],
			'rules' => $req['rules']
		]);


		for ($i = 0; $i < $this->pic_count; $i++) {
			$tmp_file = 'pic_' . ($i + 1);
			if ($request->hasFile($tmp_file)) {
				$usersRooms[$tmp_file] = $user_id . '_' . time() . '_' . $request[$tmp_file]->getClientOriginalName();
				Storage::put('public/images/' . $usersRooms[$tmp_file], $request[$tmp_file]->get());
			} else {
				$usersRooms[$tmp_file] = NULL;
			}
		}

		if ($usersRooms->save()) {
			return redirect('/getUserRoomsList');
		} else {
			return Back()->withInput();
		}

		return Back()->withInput();
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		if (!Auth::check()) {
			return response()
				->view('pages.no_log_in_recirect');
		}

		$user_room = UsersRooms::find($id);

		$arr = explode(",", $user_room['coordinates']);
		$lat = $arr[0];
		$lon = $arr[1];

		$previous_url = url()->previous();

		return view('pages.userShowRoom')
			->with(compact('previous_url'))
			->with(compact('lat'))
			->with(compact('lon'))
			->with(compact('user_room', $user_room));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		if (!Auth::check()) {
			return response()
				->view('pages.no_log_in_recirect');
		}

		$user_room = UsersRooms::find($id);

		$previous_url = url()->previous();

		return view('pages.userEditRoom')
			->with(compact('previous_url'))
			->with(compact('user_room', $user_room));

		// return view('pages.userEditRoom', compact('user_room',$user_room));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{

		if (!Auth::check()) {
			return response()
				->view('pages.no_log_in_recirect');
		}

		$validator = Validator::make($request->all(), [
			'address' => 'required|string|min:3',
			'related_address' => 'required|string|min:3',
			'google_map_url' => [
				'string',
				// function ($attribute, $value, $fail) {
				// 	$res = $this->googleMapUrlVal($value);
				// 	if (!$res) {
				// 		$fail('Please enter the whole google map url with latitude&longitude on field ' . $attribute);
				// 	}
				// },
			],
			'pic_1' => 'mimes:jpeg,png,jpg,gif,pdf|max:2048',
			'pic_2' => 'mimes:jpeg,png,jpg,gif,pdf|max:2048',
		]);


		if ($validator->fails()) {
			return redirect()->Back()->withInput()->withErrors($validator);
		}

		$userRoomOri = UsersRooms::find($id);
		$user_id = Auth::id();
		$req =  $request->all();

		$userRoomOri->address = $req['address'];
		$userRoomOri->related_address = $req['related_address'];
		$userRoomOri->google_map_url = $req['google_map_url'];
		$userRoomOri->coordinates = $this->googleMapUrlVal($req['google_map_url']);
		$userRoomOri->equipments = $req['equipments'];
		$userRoomOri->rules = $req['rules'];

		for ($i = 0; $i < $this->pic_count; $i++) {
			$tmp_file = 'pic_' . ($i + 1);
			if ($request->hasFile($tmp_file)) {
				//delete old picture
				$full_path_file = 'public/images/' . $userRoomOri[$tmp_file];
				if (Storage::exists($full_path_file)) {
					Storage::delete($full_path_file);
				} else {
					continue;
				}

				$userRoomOri->$tmp_file = $user_id . '_' . time() . '_' . $request->file($tmp_file)->getClientOriginalName();
				Storage::put('public/images/' . $userRoomOri->$tmp_file, $request->file($tmp_file)->get());
			} else {
				$userRoomOri->$tmp_file = $userRoomOri->$tmp_file;
			}
		}


		if ($userRoomOri->update()) {
			//Session::flash('message', 'Update successfully!');
			//Session::flash('alert-class', 'alert-success');
			return redirect('/getUserRoomsList');
		} else {
			//Session::flash('message', 'Data not updated!');
			//Session::flash('alert-class', 'alert-danger');
			//return redirect('/getUserRoomsList');
			return Back()->withInput();
		}

		return Back()->withInput();
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{

		if (!Auth::check()) {
			return response()
				->view('pages.no_log_in_recirect');
		}


		$user_room = UsersRooms::find($id);

		for ($i = 0; $i < $this->pic_count; $i++) {
			$full_path_file = 'public/images/' . $user_room['pic_' . ($i + 1)];

			if (Storage::exists($full_path_file)) {
				Storage::delete($full_path_file);
			} else {
				continue;
			}
		}

		UsersRooms::destroy($id);
		return redirect('/getUserRoomsList');
	}

	public function showMap()
	{
		return view('pages.showMap_1');
	}
}
