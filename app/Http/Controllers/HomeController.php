<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UsersRooms as UsersRooms;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
	    $users_rooms = UsersRooms::orderBy('created_at', 'desc')->paginate(6);
	    return view('pages.home',compact('users_rooms'));
    }

    public function index1()
    {
	    // $users_rooms = UsersRooms::orderBy('created_at', 'desc')->paginate(6);
		// return view('pages.home',compact('users_rooms'));
		return view('pages.home1');
    }

    public function show($id) 
    {    
	    $user_room = UsersRooms::find($id);
	    $previous_url = url()->previous();

	    if($user_room['coordinates']) {
		    $arr = explode(",", $user_room['coordinates']);    
		    $lat = $arr[0];
		    $lon = $arr[1];
	    } else {
		    $lat = NULL;
		    $lon = NULL;
	    }

	    return view('pages.showRoom')
		    ->with(compact('previous_url'))
		    ->with(compact('lat'))
		    ->with(compact('lon'))
		    ->with(compact('user_room',$user_room));
    }

    public function booking($id)
    {
	    return view('pages.editBookingRoom');


	    dd('id is'.$id);
	    dd('booking-1111111111');
	    $user_room = UsersRooms::find($id);
	    $previous_url = url()->previous();

	    if($user_room['coordinates']) {
		    $arr = explode(",", $user_room['coordinates']);
		    $lat = $arr[0];
		    $lon = $arr[1];
	    } else {
		    $lat = NULL;
		    $lon = NULL;
	    }

	    return view('pages.showRoom')
		    ->with(compact('previous_url'))
		    ->with(compact('lat'))
		    ->with(compact('lon'))
		    ->with(compact('user_room',$user_room));
    }


}
