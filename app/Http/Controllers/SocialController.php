<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator, Redirect, Response, File;
use Socialite;
use App\User;
use Hash;
use Session;

class SocialController extends Controller
{
    public function redirect(Request $request, $provider)
    {
        $request->session()->put('currentURL', url()->previous());
        return Socialite::driver($provider)->redirect();
    }

    public function logout()
    {
        auth()->logout();
        return back();
    }

    public function callback(Request $request, $provider)
    {
        // dd( Socialite::driver($provider)->clientSecret() );
        // dd( Socialite::driver($provider) );        
        // dd( Socialite::driver($provider)->user() );
        // dd( Socialite::driver($provider) );
        // dd('33333');

        $getInfo = Socialite::driver($provider)->user();

        $user = $this->createUser($getInfo, $provider);

        auth()->login($user);

        $sessionCurrentURL = $request->session()->get('currentURL');
        return redirect($sessionCurrentURL);
    }

    function createUser($getInfo, $provider)
    {
        $user = User::where('provider_id', $getInfo->id)->first();

        if (!$user) {
            $user = User::create([
                'name'     => $getInfo->name,
                //'email'    => $getInfo->email,
                'email'    => $getInfo->email ? $getInfo->email : $getInfo->name,
                'password' => Hash::make(uniqid()),
                'provider' => $provider,
                'provider_id' => $getInfo->id,
            ]);
        }
        return $user;
    }
}
