<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UsersRooms as UsersRooms;


use Illuminate\Support\Facades\Log;


use LINE\LINEBot;
use LINE\LINEBot\Constant\HTTPHeader;
use LINE\LINEBot\SignatureValidator;
use LINE\LINEBot\HTTPClient\CurlHTTPClient;
use LINE\LINEBot\MessageBuilder\TextMessageBuilder;
use Exception;


class RoomsController extends Controller
{
    // private $pic_count = 5;
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users_rooms = UsersRooms::orderBy('created_at', 'desc')->paginate(4);

	    if($users_rooms) {
		    //$users_rooms = UsersRooms::paginate(4);
		    //$users_rooms = UsersRooms::orderBy('created_at', 'desc')->paginate(5);
		    
		    return view('pages.roomsList',compact('users_rooms'));
	    } else {
		    return redirect()->to('/home');
		    //return redirect('/');
	    }
    }


	public function apiIndex()
	{
        $users_rooms = UsersRooms::orderBy('created_at', 'desc')->paginate(4);
        return $users_rooms;
    }
    

    
    public function apiLine_b(Request $request)
    {
        $httpClient = new CurlHTTPClient(env('LINE_BOT_CHANNEL_ACCESS_TOKEN'));
        $bot = new LINEBot($httpClient, ['channelSecret' => env('LINE_BOT_CHANNEL_SECRET')]);

        // $response = $bot->replyText(env('LINE_BOT_CHANNEL_ACCESS_TOKEN'), 'hello!');
        $textMessageBuilder = new TextMessageBuilder('hello');
        $response = $bot->replyMessage(env('LINE_BOT_CHANNEL_ACCESS_TOKEN'), $textMessageBuilder);
        if ($response->isSucceeded()) {
            echo 'Succeeded!';
            return;
        }
        
        // Failed
        echo $response->getHTTPStatus() . ' ' . $response->getRawBody();



    }

    public function apiLine(Request $request)
    {
        // Log::info('LineWebhookController for request'.$request);

        $lineAccessToken = env('LINE_BOT_CHANNEL_ACCESS_TOKEN'); //前面申請到的Channel acess token(long-lived)
        $lineChannelSecret = env('LINE_BOT_CHANNEL_SECRET'); //前面申請到的Channel secret

        $signature = $request->headers->get(HTTPHeader::LINE_SIGNATURE);
        if (!SignatureValidator::validateSignature($request->getContent(), $lineChannelSecret, $signature)) {
            return;
        }

        $httpClient = new CurlHTTPClient($lineAccessToken);
        $lineBot = new LINEBot($httpClient, ['channelSecret' => $lineChannelSecret]);

        try {
            // $events = $lineBot->parseEventRequest($request->getContent());
            $events = $lineBot->parseEventRequest($request->getContent(), $signature);
            foreach ($events as $event) {
                $replyToken = $event->getReplyToken();
                $text = $event->getText(); // 得到使用者輸入
                if($text == "test") {
                    $lineBot->replyText($replyToken, "你好");
                }
                else if($text == "test1") {
                    $lineBot->replyText($replyToken, "你要喔");
                }
                else if($text == "大白") {
                    $lineBot->replyText($replyToken, "你才大白 你全家大白");
                }
                else {
                    $lineBot->replyText($replyToken, $text); // 回復使用者輸入
                }


                $lineBot->replyText($replyToken, $text); // 回復使用者輸入
                //$textMessage = new TextMessageBuilder("你好");
                //  $lineBot->replyMessage($replyToken, $textMessage);
            }
        } catch (Exception $e) {
            return;
        }

        return;
    }







    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
