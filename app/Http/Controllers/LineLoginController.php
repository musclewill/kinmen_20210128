<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Services\LineService;


use App\User;
use Hash;
// use Session;


class LineLoginController extends Controller
{
    protected $lineService;

    public function __construct(LineService $lineService)
    {
        $this->lineService = $lineService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()
			->view('pages.line');

            return view('pages.line',compact('url'));
    }

    public function pageLine(Request $request)
    {
        $url = $this->lineService->getLoginBaseUrl();
        // dd($url);
        // $request->session()->put('currentURL', url()->previous());
        return view('pages.line')->with('url', $url);
    }
    
    public function lineLoginCallBack(Request $request)
    {        
        try {
            $error = $request->input('error', false);
            if ($error) {
                throw new Exception($request->all());
            }

            $arr1 = explode("?", $request->getrequestUri());
            $arr1 = explode("=", $arr1[1]);
            $arr1 = explode("&", $arr1[1]);
            
            // $code = $request->input('code', '');
            $code = $arr1[0];
            $response = $this->lineService->getLineToken($code);
            $user_profile = $this->lineService->getUserProfile($response['access_token']);

            // $user = $this->createUser($getInfo, $provider);
            $user = $this->createUser($user_profile, 'line');            
            auth()->login($user);

            return redirect('/');

    
            // $sessionCurrentURL = $request->session()->get('currentURL');
            // dd($sessionCurrentURL);
            // return redirect($sessionCurrentURL);
        } catch (Exception $ex) {
            Log::error($ex);
        }
    }

    function createUser($getInfo, $provider)
    {        
        $user = User::where('provider_id', $getInfo['userId'])->first();

        if (!$user) {
            
            $user = User::create([
                'name'     => $getInfo['displayName'],
                // 'email'    => $getInfo['email'] ? $getInfo['email'] : $getInfo['displayName'],
                'email'    => $getInfo['displayName'],
                'password' => Hash::make(uniqid()),
                'provider' => $provider,
                'provider_id' => $getInfo['userId'],
            ]);
        }

        return $user;
    }








    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
