<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SalonLists as SalonLists;


use Illuminate\Support\Facades\Storage;


class SalonListsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*
        $salonLists = new SalonLists;
        $salonLists = $salonLists->get()->all()->toArray();
        dd( $salonLists );
        */

        //$salonLists = SalonLists::all()->toArray();	//return array
        $salonLists = SalonLists::all();		//return object


        /*
        $url = Storage::url( $salonLists[0]->img_url_1 );
        $path = Storage::disk('local')->path($salonLists[0]->img_url_1);
        $path = Storage::disk('local')->getAdapter()->applyPathPrefix( $salonLists[0]->img_url_1 );
        var_dump($path);
        var_dump($url);
        var_dump($salonLists[0]->img_url_1);
        exit();
        */

        return view('pages.salonLists', ['salonLists' => $salonLists] );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
