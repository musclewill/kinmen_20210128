<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Validator; //驗證器
use Hash; //雜湊
use Mail; //寄信
use Socialite;
//use App\Shop\Entity\User; //使用者 Eloquent ORM Model
use App\User;

class UserAuthController extends Controller
{

    //Facebook登入
    public function facebookSignInProcess()
    {
        $redirect_url = env('FB_REDIRECT');

        return Socialite::driver('facebook')
            ->scopes(['user_friends'])
            ->redirectUrl($redirect_url)
            ->redirect();
    }


    //Facebook登入重新導向授權資料處理
    public function facebookSignInCallbackProcess()
    {
        if (request()->error == "access_denied") {
            throw new Exception('授權失敗，存取錯誤');
        }
        //依照網域產出重新導向連結 (來驗證是否為發出時同一callback)
        $redirect_url = env('FB_REDIRECT');
        //取得第三方使用者資料
        /*
        $user = Socialite::driver('facebook')->user();
        var_dump($user);
        //*/
        $FacebookUser = Socialite::driver('facebook')
            ->fields([
                'name',
                'email',
            ])
            ->redirectUrl($redirect_url)->user();

        $facebook_email = $FacebookUser->email;

        if (is_null($facebook_email)) {
            throw new Exception('未授權取得使用者 Email');
        }
        //取得 Facebook 資料
        $facebook_id = $FacebookUser->id;
        $facebook_name = $FacebookUser->name;

        //echo "facebook_id=".$facebook_id.", facebook_name=".$facebook_name;
        //*/


        //取得使用者資料是否有此Facebook_id資料
        $User = User::where('facebook_id', $facebook_id)->first();

        if (!is_null($User)) {
            //沒有綁定Facebook Id的帳號，透過Email尋找是否有此帳號
            $User = User::where('email', $facebook_email)->first();
            if (!is_null($User)) {
                //有此帳號，綁定Facebook Id
                $User->facebook_id = $facebook_id;
                $User->save();
            }
        } else    //  if(is_null($User))
        {
            //尚未註冊
            $input = [
                'email' => $facebook_email, //E-mail
                'name' => $facebook_name, //暱稱
                'password' => uniqid(), //隨機產生密碼
                'facebook_id' => $facebook_id, //Facebook ID
                //'type' => 'G', //一般使用者
            ];
            //密碼加密
            $input['password'] = Hash::make($input['password']);
            //新增會員資料
            $User = User::create($input);

            //寄送註冊通知信
            $mail_binding = [
                'name' => $input['name']
            ];

            /*
            Mail::send('email.signUpEmailNotification', $mail_binding,
                function($mail) use ($input){
                    $mail->to($input['email']);
                    $mail->from('henrychang0202@gmail.com');
                    $mail->subject('恭喜註冊 Shop Laravel 成功');
		});
	    */
        }

        //  echo "登入成功!";	

        //登入會員
        //Session記錄會員編號
        //session()->put('user_id', $User->id);

        //重新導向到原先使用者造訪頁面，沒有嘗試造訪頁則重新導向回首頁
        return redirect()->intended('/');
    }




    //Route::get('/twitter-sign-in', 'UserAuthController@twitterSignInProcess')->name('twitter-sign-in');
    //Route::get('/twitter-sign-in-callback', 'UserAuthController@twitterSignInCallbackProcess');

    // twitterSignInProcess
    public function twitterRedirect($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    // twitterSignInCallbackProcess
    public function twitterCallback($provider)
    {
        // dd('aaaaawwww');
        $getInfo = Socialite::driver($provider)->user();
        $user = $this->createUser($getInfo, $provider);
        auth()->login($user);

        return redirect()->to('/home');
    }

    function createUser($getInfo, $provider)
    {
        $user = User::where('provider_id', $getInfo->id)->first();

        if (!$user) {
            $user = User::create([
                'name'     => $getInfo->name,
                'email'    => $getInfo->email,
                'provider' => $provider,
                'provider_id' => $getInfo->id
            ]);
        }
        return $user;
    }






    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
