<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Route::get('/showRoom/{room_id}', 'HomeController@show')->name('room.show');
//Route::get('/bookingRoom/{room_id}/{address}','HomeController@booking')->name('room.booking');
Route::get('/bookingRoom/{room_id}', 'HomeController@booking')->name('room.booking');


Auth::routes();
//Route::post('/login','HomeController@index');




Route::get('/line', 'LineLoginController@pageLine');
Route::get('/line/callback/login', 'LineLoginController@lineLoginCallBack');


Route::get('/home', 'HomeController@index')->name('home');
Route::group(['prefix' => 'user'], function () {
    Route::group(['prefix' => 'auth'], function () {
        Route::get('/facebook-sign-in', 'UserAuthController@facebookSignInProcess')->name('facebooke-sign-in');
        Route::get('/facebook-sign-in-callback', 'UserAuthController@facebookSignInCallbackProcess');
    });
});

Route::get('/auth/redirect/{provider}', 'SocialController@redirect');
Route::get('/callback/{provider}', 'SocialController@callback');
Route::get('/logout', 'SocialController@logout');


// Route::get('/auth/redirect/{provider}', 'TwitterSocialController@redirect');
// Route::get('/callback/{provider}', 'TwitterSocialController@callback');



Route::get('/hello', 'HelloController@index');
Route::get('/salonLists', 'SalonListsController@index');


Route::get('/getUserLists', 'UsersController@index');

Route::get('/getUserRoomsList', 'UserRoomsController@index')->name('usersroom.list');
Route::get('/formUserRoom', 'UserRoomsController@create');
Route::post('/storeUserRoom', 'UserRoomsController@store');
Route::get('/userDeleteRoom/{users_room_id}', 'UserRoomsController@destroy')->name('usersroom.delete');
Route::get('/editUserRoom/{users_room_id}', 'UserRoomsController@edit')->name('usersroom.edit');
Route::post('/updateUserRoom/{users_room_id}', 'UserRoomsController@update')->name('usersroom.update');
Route::get('/showUserRoom/{users_room_id}', 'UserRoomsController@show')->name('usersroom.show');



Route::get('/getRoomsList', 'RoomsController@index')->name('room.list');





Route::get('/showMap', 'UserRoomsController@showMap')->name('usersroom.showMap');








