<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalonListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salon_lists', function (Blueprint $table) {
	    $table->id();
	    $table->string('title');
	    $table->string('content');
	    $table->integer('owner_id');
	    $table->string('address');
	    $table->string('img_url_1');
	    $table->string('img_url_2')->nullable();
	    $table->string('img_url_3')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salon_lists');
    }
}
