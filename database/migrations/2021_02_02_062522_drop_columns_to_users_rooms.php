<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropColumnsToUsersRooms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_rooms', function (Blueprint $table) {
            $table->dropColumn(['sun_s', 'sun_e', 'mon_s', 'mon_e', 'tue_s', 'tue_e']);
            $table->dropColumn(['wed_s', 'wed_e', 'thu_s', 'thu_e', 'fri_s', 'fri_e', 'sat_s', 'sat_e']);
            $table->string('address', 255)->nullable()->change();
            $table->string('related_address', 255)->nullable()->change();
            $table->string('google_map_url', 255)->nullable()->change();
            $table->string('equipments', 255)->nullable()->change();
            $table->string('rules', 255)->nullable()->change();

            $table->string('pic_1', 255)->nullable()->change();
            $table->string('pic_2', 255)->nullable()->change();
            $table->string('pic_3', 255)->nullable()->change();
            $table->string('pic_4', 255)->nullable()->change();
            $table->string('pic_5', 255)->nullable()->change();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_rooms', function (Blueprint $table) {
            //
        });
    }
}
