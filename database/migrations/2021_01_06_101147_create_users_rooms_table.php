<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_rooms', function (Blueprint $table) {
		$table->id();

		$table->bigInteger('user_id');
		$table->string('address');
		$table->string('related_address');
		$table->string('google_map_url');
		$table->string('equipments');
		$table->string('rules');

                $table->time('sun_s', $precision = 0);
                $table->time('sun_e', $precision = 0);		
		$table->time('mon_s', $precision = 0);
		$table->time('mon_e', $precision = 0);
		$table->time('tue_s', $precision = 0);
		$table->time('tue_e', $precision = 0);
		$table->time('wed_s', $precision = 0);
                $table->time('wed_e', $precision = 0);
		$table->time('thu_s', $precision = 0);
		$table->time('thu_e', $precision = 0);
		$table->time('fri_s', $precision = 0);
		$table->time('fri_e', $precision = 0);
		$table->time('sat_s', $precision = 0);
		$table->time('sat_e', $precision = 0);

		$table->string('pic_1');
		$table->string('pic_2');
		$table->string('pic_3');
		$table->string('pic_4');
		$table->string('pic_5');

        	$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_rooms');
    }
}
